import java.awt.image.BufferedImage;

public class Pokemon 
{
	private String idNumber;
	private String name;
	private String type1;
	private String type2;
	private String total;
	private String hp;
	private String attack;
	private String defense;
	private String specialAttack;
	private String specialDefense;
	private String speed;
	private String generation;
	private String legendary;
	private String experience;
	private String height;
	private String weight;
	private String abilitie1;
	private String abilitie2;
	private String abilitie3;
	private String move1;
	private String move2;
	private String move3;
	private String move4;
	private String move5;
	private String move6;
	private String move7;
	private String imageDirectory;
	private BufferedImage imageOfPokemon;
	
	public BufferedImage getImageOfPokemon() {
		return imageOfPokemon;
	}

	public void setImageOfPokemon(BufferedImage imageOfPokemon) {
		this.imageOfPokemon = imageOfPokemon;
	}

	public Pokemon() {
		idNumber = "";
		name = "";
		type1 = "";
		type2 = "";
		total = "";
		hp = "";
		attack = "";
		defense = "";
		specialAttack = "";
		specialDefense = "";
		speed = "";
		generation = "";
		legendary = "";
		experience = "";
		height = "";
		weight = "";
		abilitie1 = "";
		abilitie2 = "";
		abilitie3 = "";
		move1 = "";
		move2 = "";
		move3 = "";
		move4 = "";
		move5 = "";
		move6 = "";
		move7 = "";
		imageOfPokemon = null;
	}
	
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType1() {
		return type1;
	}

	public void setType1(String type1) {
		this.type1 = type1;
	}

	public String getType2() {
		return type2;
	}

	public void setType2(String type2) {
		this.type2 = type2;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getHp() {
		return hp;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}

	public String getAttack() {
		return attack;
	}

	public void setAttack(String attack) {
		this.attack = attack;
	}

	public String getDefense() {
		return defense;
	}

	public void setDefense(String defense) {
		this.defense = defense;
	}

	public String getSpecialAttack() {
		return specialAttack;
	}

	public void setSpecialAttack(String specialAttack) {
		this.specialAttack = specialAttack;
	}

	public String getSpecialDefense() {
		return specialDefense;
	}

	public void setSpecialDefense(String specialDefense) {
		this.specialDefense = specialDefense;
	}

	public String getSpeed() {
		return speed;
	}

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public String getGeneration() {
		return generation;
	}

	public void setGeneration(String generation) {
		this.generation = generation;
	}

	public String getLegendary() {
		return legendary;
	}

	public void setLegendary(String legendary) {
		this.legendary = legendary;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getAbilitie1() {
		return abilitie1;
	}

	public void setAbilitie1(String abilitie1) {
		this.abilitie1 = abilitie1;
	}

	public String getAbilitie2() {
		return abilitie2;
	}

	public void setAbilitie2(String abilitie2) {
		this.abilitie2 = abilitie2;
	}

	public String getAbilitie3() {
		return abilitie3;
	}

	public void setAbilitie3(String abilitie3) {
		this.abilitie3 = abilitie3;
	}

	public String getMove1() {
		return move1;
	}

	public void setMove1(String move1) {
		this.move1 = move1;
	}

	public String getMove2() {
		return move2;
	}

	public void setMove2(String move2) {
		this.move2 = move2;
	}

	public String getMove3() {
		return move3;
	}

	public void setMove3(String move3) {
		this.move3 = move3;
	}

	public String getMove4() {
		return move4;
	}

	public void setMove4(String move4) {
		this.move4 = move4;
	}

	public String getMove5() {
		return move5;
	}

	public void setMove5(String move5) {
		this.move5 = move5;
	}

	public String getMove6() {
		return move6;
	}

	public void setMove6(String move6) {
		this.move6 = move6;
	}

	public String getMove7() {
		return move7;
	}

	public void setMove7(String move7) {
		this.move7 = move7;
	}

	public String getImageDirectory() {
		return imageDirectory;
	}

	public void setImageDirectory(String imageDirectory) {
		this.imageDirectory = imageDirectory;
	}
		
}
