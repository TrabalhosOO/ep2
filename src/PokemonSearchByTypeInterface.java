import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class PokemonSearchByTypeInterface {

	private JFrame frame;
	private Pokedex generalPokedex = new Pokedex();
	private DefaultListModel<String> pokemonList = new DefaultListModel<String>();
	private String selectedPokemon;
	private String selectedPokemonType = "Select Type";
	
	String[] listOfPokemonsTypes = new String[]{"Selecione", "Bug", "Dark", "Dragon", "Electric", "Fairy", "Fighting", "Fire", "Flying", 
												"Ghost", "Ground", "Grass", "Ice", "Normal", "Poison", "Psychic", "Rock", "Steel", "Water" };

	
	public PokemonSearchByTypeInterface() {
		
	}

	
	private void initialize() {
		generalPokedex.readCSVFile();
		
		
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 450);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnBack = new JButton("Voltar");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonSearchInterface pokemonSearch = new PokemonSearchInterface();
				pokemonSearch.openPage();
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(350, 350, 100, 25);
		frame.getContentPane().add(btnBack);
		
		JLabel lblPokemonSearchBy = new JLabel("Pokemon Procurar por Tipo");
		lblPokemonSearchBy.setBounds(204, 12, 222, 25);
		frame.getContentPane().add(lblPokemonSearchBy);
		
		
		
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				selectedPokemonType = comboBox.getSelectedItem().toString();
				System.out.println(selectedPokemonType);
			}
		});
		comboBox.setBounds(43, 46, 261, 24);
		frame.add(comboBox);
		for(int i = 0; i < listOfPokemonsTypes.length; i++) {
			comboBox.addItem(listOfPokemonsTypes[i]);
		}
		
		JButton btnSearch = new JButton("Procurar");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
				pokemons = generalPokedex.pokemonSearchByType(selectedPokemonType); 
				for(int i = 1; i < pokemons.size(); i++) {
					pokemonList.addElement(pokemons.get(i).getName());
				}
			}
		});
		btnSearch.setBounds(326, 46, 117, 24);
		frame.add(btnSearch);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(43, 104, 225, 300);
		frame.add(scrollPane);
		
		
		JList<String> list = new JList<String>(pokemonList);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				selectedPokemon = list.getSelectedValue().toString();
			}
		});
		scrollPane.setViewportView(list);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(296, 104, 199, 133);
		frame.add(panel_1);
		
		JButton btnCheckInformation = new JButton("Abrir");
		btnCheckInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pokemon pokemon = new Pokemon();
				pokemon = generalPokedex.pokemonSearchByName(selectedPokemon);
				PokemonDataInterface pokemonData = new PokemonDataInterface(); 
				pokemonData.setPokemon(pokemon); 
				pokemonData.OpenPage();
				frame.setVisible(false);
			}
		});
		btnCheckInformation.setBounds(296, 254, 199, 25);
		frame.add(btnCheckInformation);
		
		frame.setVisible(true);
	}
	
	public void openPage() {
		initialize();
	}
}
