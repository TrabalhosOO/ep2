
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PokemonRegisterInterface {

	private JFrame frame;
	private JTextField textField;

	public PokemonRegisterInterface() {
		
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenuInterface mainMenu = new MainMenuInterface();
				mainMenu.openPage();
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(28, 12, 78, 25);
		frame.getContentPane().add(btnBack);
		
		JLabel lblPokemonRegister = new JLabel("Pokemon Register");
		lblPokemonRegister.setBounds(220, 17, 187, 15);
		frame.getContentPane().add(lblPokemonRegister);
		
		textField = new JTextField();
		textField.setBounds(73, 119, 267, 25);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setBounds(352, 119, 117, 25);
		frame.getContentPane().add(btnSearch);
		
		frame.setVisible(true);
	}

	public void openPage() {
		initialize();
	}
}
