import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Trainer extends Person{
	private String email;
	private String password;
	private Pokedex personalPokedex;
	private ArrayList<String> personalPokemonsList;
	
	public Trainer() {
		name = "";
		birthdayMonth = "";
		birthday = "";
		birthdayYear = "";
		gender = "";
		email = "";
		password = "";
		personalPokedex = new Pokedex();
		personalPokemonsList = new ArrayList<String>();
	}

	public Trainer(String name, String birthdayMonth, String birthday, String birthdayYear, String gender, String email, String password) {
		this.name = name;
		this.birthdayMonth = birthdayMonth;
		this.birthday = birthday;
		this.birthdayYear = birthdayYear;
		this.gender = gender;
		this.email = email;
		this.password = password;
		personalPokedex = new Pokedex();
		personalPokemonsList = new ArrayList<String>();
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Pokedex getPersonalPokedex() {
		return personalPokedex;
	}

	public void setPersonalPokedex(Pokedex personalPokedex) {
		this.personalPokedex = personalPokedex;
	}

	
	public ArrayList<String> getPersonalPokemonsList() {
		return personalPokemonsList;
	}

	public void setPersonalPokemonsList(ArrayList<String> personalPokemonsList) {
		this.personalPokemonsList = personalPokemonsList;
	}

	public void registerTreinerDataOnCSVFile() {
		try
	    {
	        BufferedWriter csvWriter = new BufferedWriter(new FileWriter("data/csv_files/TRAINERS_DATA.csv", true));

	        csvWriter.append(email);
	        csvWriter.append(',');
	        csvWriter.append(password);
	        csvWriter.append(',');
	        csvWriter.append(name);
	        csvWriter.append(',');
	        csvWriter.append(birthdayMonth);
	        csvWriter.append(',');
	        csvWriter.append(birthday);
	        csvWriter.append(',');
	        csvWriter.append(birthdayYear);
	        csvWriter.append(',');
	        csvWriter.append(gender);
	        csvWriter.append('\n');

	        csvWriter.flush();
	        csvWriter.close();
	    }
	    catch(IOException e)
	    {
	         e.printStackTrace();
	    } 
	}
	
	public boolean verifyIfTrainerIsRegisteredInCSVFile() {
		String line;
		String csvSplitBy = ",";
		
		try (BufferedReader  csvReader= new BufferedReader(new FileReader("data/csv_files/TRAINERS_DATA.csv"))) {

            while ((line = csvReader.readLine()) != null) {
            	String[] trainerAtributes = line.split(csvSplitBy);
      
            	if(email.intern() == trainerAtributes[0].intern()) {
            		return true;
            	}
            }
           
		}
		catch(IOException e) {
	          e.printStackTrace();
		} 
		return false;
	} 
	
	public void loadPersonalPokemonList() {
		String line;
		String csvSplitBy = ",";
		personalPokemonsList.clear();
		
		try (BufferedReader  csvReader= new BufferedReader(new FileReader("data/csv_files/TRAINERS_POKEDEX.csv"))) {
			
			while ((line = csvReader.readLine()) != null) {
            	String[] trainerPokemons = line.split(csvSplitBy);
            	
            	if(email.intern() == trainerPokemons[0].intern()) {
            			personalPokemonsList.add(trainerPokemons[1]);
            	}
            }
         
		}
		catch(IOException e) {
	          e.printStackTrace();
		}
	}
	
	public void loadPersonalPokedex() {
		Pokedex pokedexData = new Pokedex();
		pokedexData.readCSVFile();
		
		for(int i = 1; i < personalPokemonsList.size(); i++) {
			personalPokedex.addPokemon(pokedexData.pokemonSearchByName(personalPokemonsList.get(i)));
		}
	}
	
	public void addPokemonInPersonalPokedex(Pokemon pokemon) {
		//personalPokedex.addPokemon(pokemon);
		//personalPokemonsList.add(pokemon.getName());
		try {
			BufferedWriter csvWriter = new BufferedWriter(new FileWriter("data/csv_files/TRAINERS_POKEDEX.csv", true));
			
			csvWriter.append(email);
			csvWriter.append(",");
			csvWriter.append(pokemon.getName());
			csvWriter.append("\n");
			
			csvWriter.flush();
	        csvWriter.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean verifyIfPokemonIsRegisteredInPersonalPokedex(Pokemon pokemon) {
		loadPersonalPokemonList();
		for(int i = 0; i < personalPokemonsList.size(); i++) {
			if(pokemon.getName().intern() == personalPokemonsList.get(i).intern()) {
				return true;
			}
		}
		return false;
	}
}
