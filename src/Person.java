
public abstract class Person {
	protected String name;
	protected String birthdayMonth;
	protected String birthday;
	protected String birthdayYear;
	protected String gender;
	
	public Person() {
		name = "";
		birthdayMonth = "";
		birthday = "";
		birthdayYear = "";
		gender = "";
	}
	
	public Person(String name, String birthdayMonth, String birthday, String birthdayYear, String gender) {
		this.name = name;
		this.birthdayMonth = birthdayMonth;
		this.birthday = birthday;
		this.birthdayYear = birthdayYear;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthdayMonth() {
		return birthdayMonth;
	}

	public void setBirthdayMonth(String birthdayMonth) {
		this.birthdayMonth = birthdayMonth;
	}
	
	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getBirthdayYear() {
		return birthdayYear;
	}

	public void setBirthdayYear(String birthdayYear) {
		this.birthdayYear = birthdayYear;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
