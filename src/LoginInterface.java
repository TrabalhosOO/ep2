import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class LoginInterface {

	private JFrame frame;
	private JLabel labelEmail;
	private JLabel labelPassword;
	private JTextField textFieldOfEmail;
	private JPasswordField passwordField;
	private JButton buttonLogIn;
	private JButton buttonRegister;
	private JButton buttonViewPokemons;

	public LoginInterface() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);

		labelEmail = new JLabel("Email:");
		labelEmail.setBounds(30, 30, 63, 15);
		frame.getContentPane().add(labelEmail);
		
		labelPassword = new JLabel("Senha:");
		labelPassword.setBounds(30, 60, 92, 20);
		frame.getContentPane().add(labelPassword);
		
		textFieldOfEmail = new JTextField();
		textFieldOfEmail.setBounds(110, 30, 160, 25);
		frame.getContentPane().add(textFieldOfEmail);
		textFieldOfEmail.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(110, 60, 160, 25);
		frame.getContentPane().add(passwordField);
		
		buttonLogIn = new JButton("Entrar");
		buttonLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String trainerEmail = textFieldOfEmail.getText();
				String trainerPassword = passwordField.getText();
				
				if(!trainerEmail.isEmpty() && !trainerPassword.isEmpty()) {
					String line;
					String csvSplitBy = ",";
					int emailIsRegistered = 0;
					
					try (BufferedReader  csvReader= new BufferedReader(new FileReader("data/csv_files/TRAINERS_DATA.csv"))) {
			            while ((line = csvReader.readLine()) != null) {
			            	String[] trainerAtributes = line.split(csvSplitBy);
			      
			            	if(trainerEmail.intern() == trainerAtributes[0].intern()) {
			            		if(trainerPassword.intern() == trainerAtributes[1].intern()) {
			            			
			            			Trainer loggedTrainer = new Trainer();
			            			
			            			loggedTrainer.setEmail(trainerAtributes[0]);
			            			loggedTrainer.setPassword(trainerAtributes[1]);
			            			loggedTrainer.setName(trainerAtributes[2]);
			            			loggedTrainer.setBirthdayMonth(trainerAtributes[3]);
			            			loggedTrainer.setBirthday(trainerAtributes[4]);
			            			loggedTrainer.setBirthdayYear(trainerAtributes[5]);
			            			loggedTrainer.setGender(trainerAtributes[6]);
			            			
			            			MainMenuInterface mainMenu = new MainMenuInterface();
			            			mainMenu.setTrainer(loggedTrainer);
			            			mainMenu.openPage();
			            			frame.setVisible(false);
			            			emailIsRegistered = 2;
			            		}
			            		else {
			            			emailIsRegistered = 1;
			            		}
			            		
			            	}
			            }
			            if(emailIsRegistered == 1) {
			            	JOptionPane.showMessageDialog(frame, "LOGIN FAIL!\nIncorrect password!");
			            }
			            else if(emailIsRegistered == 0){
			            	JOptionPane.showMessageDialog(frame, "LOGIN FAIL!\nUnregistered Trainer!");
			            }
			            
					}
					catch(IOException e1) {
				          e1.printStackTrace();
					}
				}
				else {
					JOptionPane.showMessageDialog(frame, "Please, complete all fields!");
				}
			}
				
				
		});
		buttonLogIn.setBounds(310, 30, 124, 25);
		frame.getContentPane().add(buttonLogIn);
		
		buttonRegister = new JButton("Registrar");
		
		buttonRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				TrainerRegisterInterface register = new TrainerRegisterInterface();
				register.openPage();
				frame.setVisible(false);
				
			}
		});
		buttonRegister.setBounds(310, 60, 125, 25);
		frame.getContentPane().add(buttonRegister);
		
		buttonViewPokemons = new JButton("View Pokemons");
		
		buttonViewPokemons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonSearchInterface pokemonSearch = new PokemonSearchInterface("LOGIN");
				
				pokemonSearch.openPage();
				frame.setVisible(false);
			}
		});
		buttonViewPokemons.setBounds(172, 362, 261, 25);
		frame.getContentPane().add(buttonViewPokemons);
		
	}
	
	public void openPage() {
		frame.setVisible(true);
	}
}
