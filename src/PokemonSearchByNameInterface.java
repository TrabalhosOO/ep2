
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PokemonSearchByNameInterface {

	private JFrame frame;
	private JTextField textField;
	private Pokedex pokedex;
	private String wantedPokemonName;
	private Pokemon wantedPokemon;
	boolean pokemonFound;

	
	public PokemonSearchByNameInterface() {
	}
        
	private void initialize() {
		pokedex = new Pokedex();
		pokedex.readCSVFile();
		wantedPokemon = new Pokemon();
		pokemonFound = false;
		
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSearchPokemonBy = new JLabel("Procurar por Nome");
		lblSearchPokemonBy.setBounds(201, 12, 198, 15);
		frame.getContentPane().add(lblSearchPokemonBy);
		
		
		JLabel lblResultadoDaPesquisa = new JLabel("Resultado");
		lblResultadoDaPesquisa.setBounds(201, 130, 287, 15);
		frame.add(lblResultadoDaPesquisa);
		
		textField = new JTextField();
		textField.setBounds(56, 89, 324, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnSearch = new JButton("Procurar");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				wantedPokemonName = textField.getText();
				if(pokedex.pokemonSearchByName(wantedPokemonName) != null) {
					wantedPokemon = pokedex.pokemonSearchByName(wantedPokemonName);
					lblResultadoDaPesquisa.setText("Resultado: Pokemon Encontrado");
					pokemonFound = true;
				}
				else {
					lblResultadoDaPesquisa.setText("Resultado: Pokemon Não encotrado");
					pokemonFound = false;
				}
			}
		});
		btnSearch.setBounds(392, 86, 117, 25);
		frame.getContentPane().add(btnSearch);
		
		JButton btnNewButton = new JButton("Informações");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pokemonFound) {
					PokemonDataInterface pokemonData = new PokemonDataInterface();
					pokemonData.setPokemon(wantedPokemon);
					pokemonData.OpenPage();
					frame.setVisible(false);
				}
			}
		});
		btnNewButton.setBounds(150, 180, 269, 25);
		frame.add(btnNewButton);
		
		JButton btnBack = new JButton("Voltar");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonSearchInterface pokemonSearch = new PokemonSearchInterface();
				pokemonSearch.openPage();
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(250, 230, 81, 25);
		frame.getContentPane().add(btnBack);
		
		frame.setVisible(true);
		
	}
	

	public void openPage() {
		initialize();
	}
}
