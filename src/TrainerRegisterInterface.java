import javax.swing.JFrame;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class TrainerRegisterInterface {

	private JFrame frame;
	private JTextField textFieldOfName;
	private JTextField textFieldOfEmail;
	private JComboBox<String> comboBoxOfMonth; 
	private JComboBox<String> comboBoxOfDay;
	private JComboBox<String> comboBoxOfYear;
	private JRadioButton radioButtonOfFemaleGender;
	private JRadioButton radioButtonOfMaleGender;
	private JRadioButton radioButtonOfOtherGender;
	private ButtonGroup buttonGroupOfGender;
	private JPasswordField passwordField;
	private JPasswordField passwordFieldToConfirmPassword;
	private JLabel labelName;
	private JLabel labelEmail;
	private JLabel labelBirthday;
	private JLabel labelGender;
	private JLabel labelPassword;
	private JLabel labelConfirmPassword;
	private JButton buttonBack;
	private JButton buttonConfirm;
	private String[] months = new String[]{"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
											"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
	private String[] days = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15",
										 "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
	private String[] years = new String[]{ "1987", "1988", "1989","1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999",
										  "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009",
										  "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"};
	private String trainerName;
	private String trainerEmail;
	private String selectedMonth;
	private String selectedDay;
	private String selectedYear;
	private String selectedGender;
	private String trainerPassword;
	private String trainerConfirmPassword;
	
	public TrainerRegisterInterface() {

	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		textFieldOfName = new JTextField();
		textFieldOfName.setBounds(147, 34, 221, 19);
		frame.getContentPane().add(textFieldOfName);
		textFieldOfName.setColumns(10);
		
		textFieldOfEmail = new JTextField();
		textFieldOfEmail.setBounds(147, 65, 221, 19);
		frame.getContentPane().add(textFieldOfEmail);
		textFieldOfEmail.setColumns(10);
		
		comboBoxOfMonth = new JComboBox<String>();
		comboBoxOfMonth.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				selectedMonth = comboBoxOfMonth.getSelectedItem().toString();
			}
		});
		comboBoxOfMonth.setBounds(147, 96, 90, 19);
		frame.getContentPane().add(comboBoxOfMonth);
		for(int i = 0; i < months.length; i++) {
			comboBoxOfMonth.addItem(months[i]);
		}
		
		comboBoxOfDay = new JComboBox<String>();
		comboBoxOfDay.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				selectedDay = comboBoxOfDay.getSelectedItem().toString();
			}
		});
		comboBoxOfDay.setBounds(249, 96, 46, 19);
		frame.getContentPane().add(comboBoxOfDay);
		
		for(int i = 0; i < days.length; i++) { 
			comboBoxOfDay.addItem(days[i]);
		}
		
		comboBoxOfYear = new JComboBox<String>();
		comboBoxOfYear.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				selectedYear = comboBoxOfYear.getSelectedItem().toString();
			}
		});
		comboBoxOfYear.setBounds(308, 96, 60, 19);
		frame.getContentPane().add(comboBoxOfYear);
		for(int i = 0; i < years.length; i++) {
			comboBoxOfYear.addItem(years[i]);
		}
		
		radioButtonOfFemaleGender = new JRadioButton("Mulher");
		radioButtonOfFemaleGender.setBounds(147, 123, 76, 23);
		frame.getContentPane().add(radioButtonOfFemaleGender);
		
		radioButtonOfMaleGender = new JRadioButton("Homem");
		radioButtonOfMaleGender.setBounds(227, 123, 100, 23);
		frame.getContentPane().add(radioButtonOfMaleGender);
		
		
		buttonGroupOfGender = new ButtonGroup();
		buttonGroupOfGender.add(radioButtonOfFemaleGender);
		buttonGroupOfGender.add(radioButtonOfMaleGender);
		buttonGroupOfGender.add(radioButtonOfOtherGender);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(147, 154, 221, 19);
		frame.getContentPane().add(passwordField);
		
		passwordFieldToConfirmPassword = new JPasswordField();
		passwordFieldToConfirmPassword.setBounds(147, 185, 221, 19);
		frame.getContentPane().add(passwordFieldToConfirmPassword);
		
		labelName = new JLabel("Nome");
		labelName.setBounds(20, 36, 52, 15);
		frame.getContentPane().add(labelName);
		
		labelEmail = new JLabel("Email");
		labelEmail.setBounds(20, 67, 48, 15);
		frame.getContentPane().add(labelEmail);
		
		labelBirthday = new JLabel("Data Nascimento");
		labelBirthday.setBounds(20, 98, 150, 15);
		frame.getContentPane().add(labelBirthday);
		
		labelGender = new JLabel("Sexo");
		labelGender.setBounds(20, 127, 66, 15);
		frame.getContentPane().add(labelGender);
		
		labelPassword = new JLabel("Senha");
		labelPassword.setBounds(20, 156, 82, 15);
		frame.getContentPane().add(labelPassword);
		
		labelConfirmPassword = new JLabel("Confirmar senha");
		labelConfirmPassword.setBounds(20, 187, 128, 15);
		frame.getContentPane().add(labelConfirmPassword);
		
		buttonBack = new JButton("Voltar");
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginInterface login = new LoginInterface();
				login.openPage();
				frame.setVisible(false);
			}
		});
		buttonBack.setBounds(88, 216, 117, 25);
		frame.getContentPane().add(buttonBack);
		
		buttonConfirm = new JButton("Confirmar");
		buttonConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				trainerName = textFieldOfName.getText();
				trainerEmail = textFieldOfEmail.getText();
				if(radioButtonOfFemaleGender.isSelected()){
					selectedGender = "Famale";
				}
				else if(radioButtonOfMaleGender.isSelected()) {
					selectedGender = "Male";
				}
				else if(radioButtonOfOtherGender.isSelected()) {
					selectedGender = "Other";
				}
				trainerPassword = passwordField.getText();
				trainerConfirmPassword = passwordFieldToConfirmPassword.getText();
				registerTrainer();
			}
		});
		buttonConfirm.setBounds(227, 216, 117, 25);
		frame.getContentPane().add(buttonConfirm);
		
		frame.setVisible(true);
	}
	
	private void registerTrainer() {
		if(!trainerName.isEmpty() && !trainerEmail.isEmpty() && !selectedMonth.isEmpty() && !selectedDay.isEmpty() && 
		   !selectedYear.isEmpty() && !selectedGender.isEmpty() && !trainerPassword.isEmpty() && !trainerConfirmPassword.isEmpty()) {
			
			Trainer newTrainer = new Trainer(trainerName,
											 selectedMonth,
											 selectedDay,
											 selectedYear,
											 selectedGender,
											 trainerEmail,
											 trainerPassword);
	
			if(!newTrainer.verifyIfTrainerIsRegisteredInCSVFile()) {
				if(checkConfirmPassword()) {
					newTrainer.registerTreinerDataOnCSVFile();
					JOptionPane.showMessageDialog(frame, "Treiner successfully registered!");
					LoginInterface loginPage = new LoginInterface();
					loginPage.openPage();
					frame.setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(frame, "REGISTRATION FAIL!\nThe passwords are not equal!");
					//TrainerRegisterInterface trainerRegister = new TrainerRegisterInterface();
					//frame.setVisible(false);
					// Como setar o conteudo de um textfield?
				}
			}
			else {
				JOptionPane.showMessageDialog(frame, "REGISTRATION FAIL!\nTreiner already registered!");
				LoginInterface login = new LoginInterface();
				login.openPage();
				frame.setVisible(false);
			}
		}
		else {
			JOptionPane.showMessageDialog(frame, "REGISTRATION FAIL!\nPlease, complete all fields!");
		}
	}
	
	private boolean checkConfirmPassword() {
		if(trainerPassword.intern() == trainerConfirmPassword.intern()) {
			return true;
		}
		return false;
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void openPage() {
		initialize();
	}
}
