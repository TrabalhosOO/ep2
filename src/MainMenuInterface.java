
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainMenuInterface {

	private JFrame frame;
	private static Trainer trainer = new Trainer();
	
	public MainMenuInterface() {
		
	}

	public Trainer getTrainer() {
		return trainer;
	}


	public void setTrainer(Trainer trainer) {
		MainMenuInterface.trainer = trainer;
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 350, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		JButton btnCheckPokede = new JButton("Minha Pokédex");
		btnCheckPokede.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TrainerPokedexInterface trainerPokedex = new TrainerPokedexInterface();
				TrainerPokedexInterface.setTrainer(trainer);
				trainerPokedex.openPage();
				frame.setVisible(false);
			}
		});
		btnCheckPokede.setBounds(20, 20, 150, 25);
		frame.getContentPane().add(btnCheckPokede);
		
		JButton btnSearchPokemons = new JButton("Procurar");
		btnSearchPokemons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonSearchInterface pokemonSearch = new PokemonSearchInterface("MAINMENU");
				pokemonSearch.openPage();
				frame.setVisible(false);
			}
		});
		btnSearchPokemons.setBounds(180, 20, 150, 25);
		frame.getContentPane().add(btnSearchPokemons);
		
		
		JButton btnBack = new JButton("Sair");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginInterface login = new LoginInterface();
				login.openPage();
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(100, 130, 150, 25);
		frame.getContentPane().add(btnBack);
		
		frame.setVisible(true);
	}
	
	
	public void openPage() {
		initialize();
	}

}
