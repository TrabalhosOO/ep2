import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Pokedex {
	private ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
	
	public Pokedex() {
		
	}
	
	public ArrayList<Pokemon> getPokemons(){
		return pokemons;
	}
	
	public Pokemon pokemonSearchByName(String name) {
		for(int i = 0; i < pokemons.size(); i++) {
			if(name.intern() == pokemons.get(i).getName().intern()) {
				
				return pokemons.get(i);
			}
		}
		return null;
	}
	
	public ArrayList<Pokemon> pokemonSearchByType(String type) {
		ArrayList<Pokemon> pokemonsOfTheSameType = new ArrayList<Pokemon>();
		for(int i = 0; i < pokemons.size(); i++) {
			if(type.intern() == pokemons.get(i).getType1().intern() || type.intern() == pokemons.get(i).getType2().intern()) {
				//System.out.println("Tipo Encontrado!");
				pokemonsOfTheSameType.add(pokemons.get(i));
			}
		}
		return pokemonsOfTheSameType;
	}
	
	public void readCSVFile() {
		String csvFile = "data/csv_files/POKEMONS_DATA_1.csv";
		String line;
		String csvSplitBy = ",";	
		
		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
            	   Pokemon pokemon = new Pokemon();
            	
            	   String[] pokemonAtribute = line.split(csvSplitBy);
            	   
	               pokemon.setIdNumber(pokemonAtribute[0]);
	               pokemon.setName(pokemonAtribute[1]);
	               pokemon.setType1(pokemonAtribute[2]);
	               pokemon.setType2(pokemonAtribute[3]);
	               pokemon.setTotal(pokemonAtribute[4]);
	               pokemon.setHp(pokemonAtribute[5]);
	               pokemon.setAttack(pokemonAtribute[6]);
	               pokemon.setDefense(pokemonAtribute[7]);
	               pokemon.setSpecialAttack(pokemonAtribute[8]);
	               pokemon.setSpecialDefense(pokemonAtribute[9]);
	               pokemon.setSpeed(pokemonAtribute[10]);
	               pokemon.setGeneration(pokemonAtribute[11]);
	               pokemon.setLegendary(pokemonAtribute[12]);
	                
	               pokemons.add(pokemon);
	               
	        }
            
            csvFile = "data/csv_files/POKEMONS_DATA_2.csv";
            
            try(BufferedReader br2 = new BufferedReader(new FileReader(csvFile))){
            	
            	int cont = 0;
            
            	
            	while((line = br2.readLine()) != null) {
            		String[] pokemonAtributeFromSecondCSV = line.split(csvSplitBy);
            		
            		//System.out.println(line);
            		
            		for(int i = 2; i < pokemonAtributeFromSecondCSV.length; i++) {
            			switch(i) {
            				case 2:
            					pokemons.get(cont).setExperience(pokemonAtributeFromSecondCSV[2]);
            					break;
            				case 3:
            					pokemons.get(cont).setHeight(pokemonAtributeFromSecondCSV[3]);
            					break;
            				case 4:
            					pokemons.get(cont).setWeight(pokemonAtributeFromSecondCSV[4]);
            					break;
            				case 5:
            					pokemons.get(cont).setAbilitie1(pokemonAtributeFromSecondCSV[5]);
            					break;
            				case 6:
            					pokemons.get(cont).setAbilitie2(pokemonAtributeFromSecondCSV[6]);
            					break;
            				case 7:
            					pokemons.get(cont).setAbilitie3(pokemonAtributeFromSecondCSV[7]);
            					break;
            				case 8:
            					pokemons.get(cont).setMove1(pokemonAtributeFromSecondCSV[8]);
            					break;
            				case 9:
            					pokemons.get(cont).setMove2(pokemonAtributeFromSecondCSV[9]);
            					break;
            				case 10:
            					pokemons.get(cont).setMove3(pokemonAtributeFromSecondCSV[10]);
            					break;
            				case 11:
            					pokemons.get(cont).setMove4(pokemonAtributeFromSecondCSV[11]);
            					break;
            				case 12:
            					pokemons.get(cont).setMove5(pokemonAtributeFromSecondCSV[12]);
            					break;
            				case 13:
            					pokemons.get(cont).setMove6(pokemonAtributeFromSecondCSV[13]);
            					break;
            				case 14:
            					pokemons.get(cont).setMove7(pokemonAtributeFromSecondCSV[14]);
            					break;
            			}
            		}
            		
            		System.out.println(pokemons.get(cont).getName());
            		cont++;
            	}
            }catch(IOException e) {
  	          e.printStackTrace();
  	      	}        

	     } catch (IOException e) {
	          e.printStackTrace();
	      	}
	}
	
	public void SavePokemonImage(){
		String ImageDirectory;
		String pokemonName;
		BufferedImage pokemonImage = null;
		
		for(int i = 1; i < pokemons.size(); i++) {
			pokemonName = pokemons.get(i).getName().toLowerCase();
			ImageDirectory = "data/images/" + pokemonName + ".png";
			try {
				pokemonImage = ImageIO.read(new File(ImageDirectory));
				pokemons.get(i).setImageDirectory(ImageDirectory);
				pokemons.get(i).setImageOfPokemon(pokemonImage);
				System.out.println("Deu certo");
			}catch(IOException e){
				System.out.println("Falha na leitura da imagem");
			}
		}
		
	}
	
	public void addPokemon(Pokemon pokemon) {
		pokemons.add(pokemon);
	}
	
	public void ShowPokemons() {
		
		for(int i = 0; i < pokemons.size(); i++) {
			System.out.println(pokemons.get(i).getName());
			System.out.println(pokemons.get(i).getIdNumber());
			System.out.println(pokemons.get(i).getType1());
			System.out.println(pokemons.get(i).getType2());
			System.out.println(pokemons.get(i).getTotal());
			System.out.println(pokemons.get(i).getHp());
			System.out.println(pokemons.get(i).getAttack());
			System.out.println(pokemons.get(i).getDefense());
			System.out.println(pokemons.get(i).getSpecialAttack());
			System.out.println(pokemons.get(i).getSpecialDefense());
			System.out.println(pokemons.get(i).getSpeed());
			System.out.println(pokemons.get(i).getGeneration());
			System.out.println(pokemons.get(i).getLegendary());
			System.out.println(pokemons.get(i).getExperience());
			System.out.println(pokemons.get(i).getHeight());
			System.out.println(pokemons.get(i).getWeight());
			System.out.println(pokemons.get(i).getAbilitie1());
			System.out.println(pokemons.get(i).getAbilitie2());
			System.out.println(pokemons.get(i).getAbilitie3());
			System.out.println(pokemons.get(i).getMove1());
			System.out.println("__________________________");
		}
	}
}
