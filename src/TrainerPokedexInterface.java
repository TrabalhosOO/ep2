import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.ImageIcon;

public class TrainerPokedexInterface{

	private JFrame frame;
	private JButton buttonBack;
	private JLabel labelTrainersName;
	private JScrollPane scrollPaneOfPokemonList;
	private JList<String> listOfPokemons;
	private JPanel panelOfPokemonImage;
	private JButton buttonViewInformation;
	private JButton buttonSearchByName;
	private JLabel labelSearchInYourPokedex;
	private JButton buttonSearchByType;
	private static Trainer trainer = null;
	private String selectedPokemon;
	private Pokedex pokedex = new Pokedex();
        private String temp;
        private String sj;
        private ImageIcon icon;
	
	public TrainerPokedexInterface() {
		
	}

	public static Trainer getTrainer() {
		return trainer;
	}

	public static void setTrainer(Trainer trainer) {
		TrainerPokedexInterface.trainer = trainer;
	}

	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		buttonBack = new JButton("Voltar");
		
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenuInterface mainMenuScreen = new MainMenuInterface();
				mainMenuScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonBack.setBounds(250, 550, 100, 25);
		frame.getContentPane().add(buttonBack);
		
		labelTrainersName = new JLabel("Pokedex de " + trainer.getName());
		
		labelTrainersName.setBounds(220, 30, 343, 37);
		frame.getContentPane().add(labelTrainersName);
		
		scrollPaneOfPokemonList = new JScrollPane();
		scrollPaneOfPokemonList.setBounds(170, 60, 287, 411);
		frame.getContentPane().add(scrollPaneOfPokemonList);
		
		DefaultListModel<String> trainerPokemonsList = new DefaultListModel<>();
		ArrayList<String> pokemons = new ArrayList<>();
		trainer.loadPersonalPokemonList();
		pokemons = trainer.getPersonalPokemonsList();
		for(int i = 0; i < pokemons.size(); i++) {
			System.out.println(pokemons.get(i));
			trainerPokemonsList.addElement(pokemons.get(i));
		}
		listOfPokemons = new JList<String>(trainerPokemonsList);
		
		listOfPokemons.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				selectedPokemon = listOfPokemons.getSelectedValue().toString();
                
                                temp = selectedPokemon;
               sj = "data/images/" + temp +".png";
                 System.out.printf(sj);
                 System.out.printf("\n");
                 icon = new ImageIcon(sj);
                  JLabel image = new JLabel(icon);
                image.setBounds(450, 5, 1, 1);
                image.setSize(100, 100);
                image.setVisible(true);
                frame.getContentPane().add(image);
			}
		});
		scrollPaneOfPokemonList.setViewportView(listOfPokemons);

                JLabel image = new JLabel(icon);
                image.setBounds(450, 5, 1, 1);
                image.setSize(100, 100);
                image.setVisible(true);
                frame.getContentPane().add(image);
		
		buttonViewInformation = new JButton("Abrir");
		
		buttonViewInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pokedex.readCSVFile();
				Pokemon pokemon = new Pokemon();
				pokemon = pokedex.pokemonSearchByName(selectedPokemon);
				PokemonDataInterface pokemonDataScreen = new PokemonDataInterface();
				pokemonDataScreen.setPokemon(pokemon); 
				
				pokemonDataScreen.OpenPage();
				frame.setVisible(false);
			}
		});
		buttonViewInformation.setBounds(250, 500, 90, 25);
		frame.getContentPane().add(buttonViewInformation);
		
		
		
		
		frame.setVisible(true);
	}
	
	public void openPage() {
		initialize();
	}
}
