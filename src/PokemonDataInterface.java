import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class PokemonDataInterface {

	private JFrame frame;
	private Pokemon pokemon = new Pokemon();
        
        
	
	public PokemonDataInterface() {
		
	}	
	
	public Pokemon getPokemon() {
		return pokemon;
	}


	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}
        
        
        
        
        
       


	private void initialize() {
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null); 
		frame.getContentPane().setLayout(null);
                
                        

                
		
		JButton buttonBack = new JButton("Voltar");
		buttonBack.addActionListener((ActionEvent e) -> {
                    PokemonSearchInterface pokemonSearch = new PokemonSearchInterface();
                    pokemonSearch.openPage();
                    frame.setVisible(false);
                });
		buttonBack.setBounds(230, 575, 100, 25);
		frame.getContentPane().add(buttonBack);
		
		JLabel lblNewLabel = new JLabel("# " + pokemon.getIdNumber());
		lblNewLabel.setBounds(30, 21, 70, 15);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblPokemonsName = new JLabel(pokemon.getName());
		lblPokemonsName.setBounds(30, 51, 171, 15);
		frame.getContentPane().add(lblPokemonsName);
		
		if("True".equals(pokemon.getLegendary().intern())) {
			JLabel lblLegendary = new JLabel("Lendário");
			lblLegendary.setBounds(222, 128, 76, 15);
			frame.getContentPane().add(lblLegendary);
		}
		
		
                String temp = pokemon.getName();
                 String sj = "data/images/" + temp +".png";
                // System.out.printf(sj);
                  ImageIcon icon = new ImageIcon(sj);
               
                
                JLabel image = new JLabel(icon);
                image.setBounds(470, 5, 100, 100);
                image.setSize(100, 100);
                image.setVisible(true);
                frame.getContentPane().add(image);
               
   
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setBounds(120, 20, 335, 99);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(12, 31, 132, 28);
		panel_1.add(panel_2);
		
		JLabel lblPokemonsType = new JLabel(pokemon.getType1());
		panel_2.add(lblPokemonsType);
		
		JLabel lblFirstType = new JLabel("Primary Type");
		lblFirstType.setBounds(22, 71, 92, 15);
		panel_1.add(lblFirstType);
		
		if(!"".equals(pokemon.getType2().intern())) {
			JPanel panel_3 = new JPanel();
			panel_3.setBackground(Color.WHITE);
			panel_3.setBounds(180, 31, 132, 28);
			panel_1.add(panel_3);
			
			JLabel lblPokemonsType_1 = new JLabel(pokemon.getType2());
			panel_3.add(lblPokemonsType_1);
			
			JLabel lblSecon = new JLabel("Secondary Type");
			lblSecon.setBounds(190, 71, 113, 15);
			panel_1.add(lblSecon);
		}
		
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.LIGHT_GRAY);
		panel_4.setBounds(120, 161, 350, 50);
		frame.getContentPane().add(panel_4);
		panel_4.setLayout(null);
		
		JLabel lblExperience = new JLabel("Experience: " + pokemon.getExperience());
		lblExperience.setBounds(12, 15, 181, 21);
		panel_4.add(lblExperience);
		
		JLabel lblHeight = new JLabel("Height: " + pokemon.getHeight());
		lblHeight.setBounds(150, 15, 171, 21);
		panel_4.add(lblHeight);
		
		JLabel lblWeight = new JLabel("Weight: " + pokemon.getWeight());
		lblWeight.setBounds(250, 15, 198, 21);
		panel_4.add(lblWeight);
		
		JLabel label = new JLabel("Estatisticas");
		label.setBounds(16, 222, 100, 15);
		frame.getContentPane().add(label);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.LIGHT_GRAY);
		panel_5.setBounds(6, 249, 567, 87);
		frame.getContentPane().add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblHp = new JLabel("HP: " + pokemon.getHp());
		lblHp.setBounds(12, 12, 122, 15);
		panel_5.add(lblHp);
		
		JLabel lblSpeed = new JLabel("Speed: " + pokemon.getSpeed());
		lblSpeed.setBounds(12, 35, 134, 15);
		panel_5.add(lblSpeed);
		
		JLabel lblAttack = new JLabel("Attack: " + pokemon.getAttack());
		lblAttack.setBounds(199, 12, 134, 15);
		panel_5.add(lblAttack);
		
		JLabel lblDefense = new JLabel("Defense: " + pokemon.getDefense());
		lblDefense.setBounds(199, 35, 134, 15);
		panel_5.add(lblDefense);
		
		JLabel lblSpecialDefense = new JLabel("Special Defense: " + pokemon.getSpecialDefense());
		lblSpecialDefense.setBounds(392, 35, 163, 15);
		panel_5.add(lblSpecialDefense);
		
		JLabel lblSpecialAttack = new JLabel("Special Attack: " + pokemon.getSpecialDefense());
		lblSpecialAttack.setBounds(392, 12, 163, 15);
		panel_5.add(lblSpecialAttack);
		
		JLabel lblTotal = new JLabel("Total: " + pokemon.getTotal());
		lblTotal.setBounds(392, 62, 149, 15);
		panel_5.add(lblTotal);
		
		JLabel lblAbilities = new JLabel("Habilidades");
		lblAbilities.setBounds(16, 348, 100, 15);
		frame.getContentPane().add(lblAbilities);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.LIGHT_GRAY);
		panel_6.setBounds(6, 375, 239, 183);
		frame.getContentPane().add(panel_6);
		panel_6.setLayout(null);
		
		
		if(!"".equals(pokemon.getAbilitie1().intern())){
			
			JPanel panel_7 = new JPanel();
			panel_7.setBounds(12, 12, 215, 20);
			panel_6.add(panel_7);
			
			JLabel lblAbility_3 = new JLabel(pokemon.getAbilitie1());
			panel_7.add(lblAbility_3);
			
			JLabel lblAbility = new JLabel("Ability 1");
			lblAbility.setBounds(12, 38, 70, 15);
			panel_6.add(lblAbility);

		}
			
		if(!"".equals(pokemon.getAbilitie2().intern())) {
			JPanel panel_8 = new JPanel();
			panel_8.setBounds(12, 65, 215, 20);
			panel_6.add(panel_8);
			
			JLabel lblAbility_4 = new JLabel(pokemon.getAbilitie2());
			panel_8.add(lblAbility_4);
			
			JLabel lblAbility_1 = new JLabel("Ability 2");
			lblAbility_1.setBounds(12, 90, 70, 15);
			panel_6.add(lblAbility_1);
		}
		
		if(pokemon.getAbilitie3().intern() != "") {
			JPanel panel_9 = new JPanel();
			panel_9.setBounds(12, 117, 215, 20);
			panel_6.add(panel_9);
			
			JLabel lblAbility_5 = new JLabel(pokemon.getAbilitie3());
			panel_9.add(lblAbility_5);
			
			JLabel lblAbility_2 = new JLabel("Ability 3");
			lblAbility_2.setBounds(12, 149, 70, 15);
			panel_6.add(lblAbility_2);
		}
		
		
		JPanel panel_10 = new JPanel();
		panel_10.setBackground(Color.LIGHT_GRAY);
		panel_10.setBounds(265, 375, 308, 183);
		frame.getContentPane().add(panel_10);
		panel_10.setLayout(null);
		
		if(pokemon.getMove1().intern() != "") {
			JPanel panel_7 = new JPanel();
			panel_7.setBounds(12, 12, 141, 22);
			panel_10.add(panel_7);
			
			JLabel lblMove_1 = new JLabel(pokemon.getMove1());
			panel_7.add(lblMove_1);
		}
		
		if(pokemon.getMove2().intern() != "") {
			JPanel panel_8 = new JPanel();
			panel_8.setBounds(12, 52, 141, 22);
			panel_10.add(panel_8);
			
			JLabel lblMove_3 = new JLabel(pokemon.getMove2());
			panel_8.add(lblMove_3);
		}
		
		if(pokemon.getMove3().intern() != "") {
			JPanel panel_9 = new JPanel();
			panel_9.setBounds(12, 93, 141, 22);
			panel_10.add(panel_9);
			
			JLabel lblMove = new JLabel(pokemon.getMove3());
			panel_9.add(lblMove);
		}
		
		if(pokemon.getMove4().intern() != "") {
			JPanel panel_11 = new JPanel();
			panel_11.setBounds(12, 132, 141, 22);
			panel_10.add(panel_11);
			
			JLabel lblMove_2 = new JLabel(pokemon.getMove4());
			panel_11.add(lblMove_2);
		}
		
		if(pokemon.getMove5().intern() != "") {
			JPanel panel_12 = new JPanel();
			panel_12.setBounds(165, 12, 131, 22);
			panel_10.add(panel_12);
			
			JLabel lblMove_4 = new JLabel(pokemon.getMove5());
			panel_12.add(lblMove_4);
		}
		
		if(pokemon.getMove6().intern() != "") {
			JPanel panel_13 = new JPanel();
			panel_13.setBounds(165, 52, 131, 22);
			panel_10.add(panel_13);
			
			JLabel lblMove_5 = new JLabel(pokemon.getMove6());
			panel_13.add(lblMove_5);
		}
		
		if(pokemon.getMove7().intern() != "") {
			JPanel panel_14 = new JPanel();
			panel_14.setBounds(165, 93, 131, 22);
			panel_10.add(panel_14);
			
			JLabel lblMove_6 = new JLabel(pokemon.getMove7());
			panel_14.add(lblMove_6);
		}
		
		JLabel lblMoves = new JLabel("Ataques");
		lblMoves.setBounds(271, 348, 70, 15);
		frame.getContentPane().add(lblMoves);
		
		frame.setVisible(true);
	}
	
	
	public void OpenPage() {
		initialize();
	}
}
