import javax.swing.JFrame;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JScrollPane;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class PokemonSearchInterface {

	protected JFrame frame;
	private Pokedex generalPokedex = new Pokedex();
	private DefaultListModel<String> pokemonList = new DefaultListModel<String>();
	private String selectedPokemon;
	private static String previousPage;

	public PokemonSearchInterface() {
		
	}
	
	public PokemonSearchInterface(String previousPage) {
		this.previousPage = previousPage;
	}
	
	public String getPreviousPage() {
		return previousPage;
	}

	public void setPreviousPage(String previousPage) {
		this.previousPage = previousPage;
	}

	private void initialize() {
		generalPokedex.readCSVFile();
		
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		JButton btnBack = new JButton("Voltar");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(previousPage.intern() == "LOGIN") {
					LoginInterface login = new LoginInterface();
					login.openPage();
					frame.setVisible(false);
				}
				else if(previousPage.intern() == "MAINMENU") {
					MainMenuInterface mainMenu = new MainMenuInterface();
					mainMenu.openPage();
					frame.setVisible(false);
				}
				
			}
		});
		btnBack.setBounds(250, 550, 81, 25);
		frame.getContentPane().add(btnBack);
		
		
		ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
		pokemons = generalPokedex.getPokemons();
		for(int i = 1; i < pokemons.size(); i++) {
			pokemonList.addElement(pokemons.get(i).getName());
		}
		JList<String> list = new JList<String>(pokemonList);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (list.getSelectedValue() != null) {
					selectedPokemon = list.getSelectedValue().toString();
					
				}
			}
		});
		frame.getContentPane().add(list);
		list.setBounds(130, 150, 291, 340);

		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBounds(130, 110, 306, 329);
		frame.getContentPane().add(scrollPane);
		
		JButton btnSearchByName = new JButton("Procurar por Nome");
		btnSearchByName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonSearchByNameInterface pokemonSearch = new PokemonSearchByNameInterface();
				pokemonSearch.openPage();
				frame.setVisible(false);
			}
		});
		btnSearchByName.setBounds(10, 30, 298, 25);
		frame.getContentPane().add(btnSearchByName);
		
		JButton btnSearchByType = new JButton("Procurar por Tipo");
		btnSearchByType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PokemonSearchByTypeInterface pokemonSearch = new PokemonSearchByTypeInterface();
				pokemonSearch.openPage();
				frame.setVisible(false);
			}
		});
		btnSearchByType.setBounds(300, 30, 298, 25);
		frame.getContentPane().add(btnSearchByType);
		
		JButton btnCheckPokemonsInformation = new JButton("Abrir");
		btnCheckPokemonsInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Pokemon pokemon = new Pokemon();
				pokemon = generalPokedex.pokemonSearchByName(selectedPokemon);
				PokemonDataInterface pokemonData = new PokemonDataInterface();
				pokemonData.setPokemon(pokemon); 
				pokemonData.OpenPage();
				frame.setVisible(false);
			}
		});
		btnCheckPokemonsInformation.setBounds(180, 470, 206, 25);
		frame.getContentPane().add(btnCheckPokemonsInformation);
		
		frame.setVisible(true);
	}

	public void openPage() {
		initialize();
	}
}
